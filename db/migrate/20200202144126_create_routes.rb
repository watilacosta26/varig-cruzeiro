class CreateRoutes < ActiveRecord::Migration[5.2]
  def change
    create_table :routes do |t|
      t.string :flight
      t.references :departure
      t.references :arrival
      t.references :alternative
      t.string :duration
      t.string :description

      t.timestamps
    end
  end
end
