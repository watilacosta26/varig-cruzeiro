
namespace :setup do
  task airpots: :environment do
    require 'csv'
    puts 'Criando aeroportos...'
    path = "#{Rails.root}/cargas"

    CSV.foreach("#{path}/airports.csv") do |row|
      puts row[0][1]
      Airport.create!(
        icao: row[0],
        description:  row[1],
        lat:  row[2],
        long: row[3]
      )
      puts Airport.last.icao
    end
  end

end
