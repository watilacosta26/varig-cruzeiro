class Admin::RoutesController < Admin::AdminController
  before_action :set_route, only: [:edit, :update, :show, :destory]

  def index
    @routes = Route.all.page params[:page]
  end

  def edit
  end

  private

  def set_route
    @route = Route.find(params[:id])
  end
end
