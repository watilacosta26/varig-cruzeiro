# == Schema Information
#
# Table name: aircraft_types
#
#  id          :bigint           not null, primary key
#  description :string
#  created_at  :datetime         not null
#  updated_at  :datetime         not null
#

class AircraftType < ApplicationRecord
end
