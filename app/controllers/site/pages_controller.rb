class Site::PagesController < ApplicationController
  def index
    @last_pilots = Pilot.order(created_at: :desc).limit(5)
  end

  def pilots
    @pilots = Pilot.order(callsign: :asc)
  end
end
