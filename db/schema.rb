# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 2020_02_04_195140) do

  # These are extensions that must be enabled in order to support this database
  enable_extension "plpgsql"

  create_table "aircraft_types", force: :cascade do |t|
    t.string "description"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "aircrafts", force: :cascade do |t|
    t.bigint "aircraft_type_id"
    t.string "tail_number"
    t.string "location"
    t.string "hub"
    t.string "hours"
    t.string "name"
    t.boolean "booked"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["aircraft_type_id"], name: "index_aircrafts_on_aircraft_type_id"
  end

  create_table "airports", force: :cascade do |t|
    t.string "icao"
    t.string "description"
    t.string "lat"
    t.string "long"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "information", force: :cascade do |t|
    t.string "title"
    t.string "description"
    t.boolean "active"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "pilots", force: :cascade do |t|
    t.string "email", default: "", null: false
    t.string "encrypted_password", default: "", null: false
    t.string "reset_password_token"
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.string "callsign"
    t.bigint "rank_id"
    t.string "name"
    t.string "city"
    t.string "country"
    t.integer "status"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.string "ivao"
    t.string "vatsim"
    t.decimal "hours"
    t.string "location"
    t.integer "profile"
    t.index ["email"], name: "index_pilots_on_email", unique: true
    t.index ["rank_id"], name: "index_pilots_on_rank_id"
    t.index ["reset_password_token"], name: "index_pilots_on_reset_password_token", unique: true
  end

  create_table "ranks", force: :cascade do |t|
    t.string "description"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "routes", force: :cascade do |t|
    t.string "flight"
    t.bigint "departure_id"
    t.bigint "arrival_id"
    t.bigint "alternative_id"
    t.string "duration"
    t.string "description"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["alternative_id"], name: "index_routes_on_alternative_id"
    t.index ["arrival_id"], name: "index_routes_on_arrival_id"
    t.index ["departure_id"], name: "index_routes_on_departure_id"
  end

  add_foreign_key "aircrafts", "aircraft_types"
end
