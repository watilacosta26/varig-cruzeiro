class Admin::AdminController < ApplicationController
  before_action :authenticate_pilot!

  layout 'admin'
end
