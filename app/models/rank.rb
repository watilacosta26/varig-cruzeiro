# == Schema Information
#
# Table name: ranks
#
#  id          :bigint           not null, primary key
#  description :string
#  created_at  :datetime         not null
#  updated_at  :datetime         not null
#

class Rank < ApplicationRecord
  has_many :pilots
end
