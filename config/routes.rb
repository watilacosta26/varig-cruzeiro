# == Route Map
#
#                    Prefix Verb   URI Pattern                                                                              Controller#Action
#         new_pilot_session GET    /pilots/sign_in(.:format)                                                                devise/sessions#new
#             pilot_session POST   /pilots/sign_in(.:format)                                                                devise/sessions#create
#     destroy_pilot_session DELETE /pilots/sign_out(.:format)                                                               devise/sessions#destroy
#        new_pilot_password GET    /pilots/password/new(.:format)                                                           devise/passwords#new
#       edit_pilot_password GET    /pilots/password/edit(.:format)                                                          devise/passwords#edit
#            pilot_password PATCH  /pilots/password(.:format)                                                               devise/passwords#update
#                           PUT    /pilots/password(.:format)                                                               devise/passwords#update
#                           POST   /pilots/password(.:format)                                                               devise/passwords#create
# cancel_pilot_registration GET    /pilots/cancel(.:format)                                                                 devise/registrations#cancel
#    new_pilot_registration GET    /pilots/sign_up(.:format)                                                                devise/registrations#new
#   edit_pilot_registration GET    /pilots/edit(.:format)                                                                   devise/registrations#edit
#        pilot_registration PATCH  /pilots(.:format)                                                                        devise/registrations#update
#                           PUT    /pilots(.:format)                                                                        devise/registrations#update
#                           DELETE /pilots(.:format)                                                                        devise/registrations#destroy
#                           POST   /pilots(.:format)                                                                        devise/registrations#create
#                      root GET    /                                                                                        site/pages#index
#     site_backoffice_index GET    /site/backoffice/index(.:format)                                                         site/backoffice#index
#               admin_ranks GET    /admin/ranks(.:format)                                                                   admin/ranks#index
#                           POST   /admin/ranks(.:format)                                                                   admin/ranks#create
#            new_admin_rank GET    /admin/ranks/new(.:format)                                                               admin/ranks#new
#           edit_admin_rank GET    /admin/ranks/:id/edit(.:format)                                                          admin/ranks#edit
#                admin_rank GET    /admin/ranks/:id(.:format)                                                               admin/ranks#show
#                           PATCH  /admin/ranks/:id(.:format)                                                               admin/ranks#update
#                           PUT    /admin/ranks/:id(.:format)                                                               admin/ranks#update
#                           DELETE /admin/ranks/:id(.:format)                                                               admin/ranks#destroy
#        rails_service_blob GET    /rails/active_storage/blobs/:signed_id/*filename(.:format)                               active_storage/blobs#show
# rails_blob_representation GET    /rails/active_storage/representations/:signed_blob_id/:variation_key/*filename(.:format) active_storage/representations#show
#        rails_disk_service GET    /rails/active_storage/disk/:encoded_key/*filename(.:format)                              active_storage/disk#show
# update_rails_disk_service PUT    /rails/active_storage/disk/:encoded_token(.:format)                                      active_storage/disk#update
#      rails_direct_uploads POST   /rails/active_storage/direct_uploads(.:format)                                           active_storage/direct_uploads#create

Rails.application.routes.draw do
  namespace :admin do
    get 'routes/index'
  end
  devise_for :pilots
  
  root 'site/pages#index'
  
  namespace :site do
    get 'pages/pilots'
  end
  
  namespace :admin do
    root 'home#index'
    resources :ranks
    resources :pilots
    resources :airports
    resources :aircrafts
    resources :aircraft_types
    resources :routes
  end
end
