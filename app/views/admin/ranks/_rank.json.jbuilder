json.extract! rank, :id, :description, :created_at, :updated_at
json.url rank_url(rank, format: :json)
