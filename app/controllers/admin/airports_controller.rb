class Admin::AirportsController < Admin::AdminController
  def index
   @airports = Airport.all.page params[:page]
  end
end
