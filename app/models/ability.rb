class Ability
  include CanCan::Ability

  def initialize(pilot)
    alias_action :create, :read, :update, :destroy, to: :crud

    pilot ||= Pilot.new
    
    if pilot.present?
      if pilot.staff?
        can :crud, Rank, Route
        can :crud, Pilot, profile: :normal
      end
      
      if pilot.admin?
        can :manage, :all
      end
    end
  end
end