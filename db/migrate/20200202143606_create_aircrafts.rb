class CreateAircrafts < ActiveRecord::Migration[5.2]
  def change
    create_table :aircrafts do |t|
      t.references :aircraft_type, foreign_key: true
      t.string :tail_number
      t.string :location
      t.string :hub
      t.string :hours
      t.string :name
      t.boolean :booked

      t.timestamps
    end
  end
end
