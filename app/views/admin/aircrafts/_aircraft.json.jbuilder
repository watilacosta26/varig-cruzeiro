json.extract! aircraft, :id, :aricraft_type_id, :tail_number, :location, :hub, :hours, :name, :booked, :created_at, :updated_at
json.url aircraft_url(aircraft, format: :json)
