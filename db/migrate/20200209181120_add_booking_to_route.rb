class AddBookingToRoute < ActiveRecord::Migration[5.2]
  def change
    add_column :routes, :booking, :boolean
  end
end
