class Admin::PilotsController < Admin::AdminController
  before_action :set_pilot, only: [:edit]

  def index
    @pilots = Pilot.order(callsign: :asc)
  end

  def edit
  end

  private 
  
  def set_pilot
    @pilot = Pilot.find(params[:id])
  end
end
