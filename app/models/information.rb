# == Schema Information
#
# Table name: information
#
#  id          :bigint           not null, primary key
#  active      :boolean
#  description :string
#  title       :string
#  created_at  :datetime         not null
#  updated_at  :datetime         not null
#

class Information < ApplicationRecord
end
