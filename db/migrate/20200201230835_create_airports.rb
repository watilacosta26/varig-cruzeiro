class CreateAirports < ActiveRecord::Migration[5.2]
  def change
    create_table :airports do |t|
      t.string :icao
      t.string :description
      t.string :lat
      t.string :long

      t.timestamps
    end
  end
end
