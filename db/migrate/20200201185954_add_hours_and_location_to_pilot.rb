class AddHoursAndLocationToPilot < ActiveRecord::Migration[5.2]
  def change
    add_column :pilots, :hours, :decimal
    add_column :pilots, :location, :string
  end
end
