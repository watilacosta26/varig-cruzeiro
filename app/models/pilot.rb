# == Schema Information
#
# Table name: pilots
#
#  id                     :bigint           not null, primary key
#  callsign               :string
#  city                   :string
#  country                :string
#  email                  :string           default(""), not null
#  encrypted_password     :string           default(""), not null
#  hours                  :decimal(, )
#  ivao                   :string
#  location               :string
#  name                   :string
#  profile                :integer
#  remember_created_at    :datetime
#  reset_password_sent_at :datetime
#  reset_password_token   :string
#  status                 :integer
#  vatsim                 :string
#  created_at             :datetime         not null
#  updated_at             :datetime         not null
#  rank_id                :bigint
#
# Indexes
#
#  index_pilots_on_email                 (email) UNIQUE
#  index_pilots_on_rank_id               (rank_id)
#  index_pilots_on_reset_password_token  (reset_password_token) UNIQUE
#

class Pilot < ApplicationRecord
  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable, :trackable and :omniauthable
  devise :database_authenticatable, :registerable,
         :recoverable, :rememberable, :validatable

  belongs_to :rank, optional: true

  enum status: [:approved, :disapproved]
  enum profile: [:admin, :staff, :normal]

  validates :name, presence: true, length: { in: 2..50 }
  validates :ivao, :vatsim, length: {maximum: 10}
  validates :city, :country, length: { maximum: 60 }
  validates :callsign, uniqueness: true
end
