class AddIvaoVatsimToPilot < ActiveRecord::Migration[5.2]
  def change
    add_column :pilots, :ivao, :string
    add_column :pilots, :vatsim, :string
  end
end
