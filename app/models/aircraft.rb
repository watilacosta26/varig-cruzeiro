# == Schema Information
#
# Table name: aircrafts
#
#  id               :bigint           not null, primary key
#  booked           :boolean
#  hours            :string
#  hub              :string
#  location         :string
#  name             :string
#  tail_number      :string
#  created_at       :datetime         not null
#  updated_at       :datetime         not null
#  aircraft_type_id :bigint
#
# Indexes
#
#  index_aircrafts_on_aircraft_type_id  (aircraft_type_id)
#
# Foreign Keys
#
#  fk_rails_...  (aircraft_type_id => aircraft_types.id)
#

class Aircraft < ApplicationRecord
  belongs_to :aircraft_type
end
