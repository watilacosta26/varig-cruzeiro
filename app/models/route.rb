# == Schema Information
#
# Table name: routes
#
#  id             :bigint           not null, primary key
#  description    :string
#  duration       :string
#  flight         :string
#  created_at     :datetime         not null
#  updated_at     :datetime         not null
#  alternative_id :bigint
#  arrival_id     :bigint
#  departure_id   :bigint
#
# Indexes
#
#  index_routes_on_alternative_id  (alternative_id)
#  index_routes_on_arrival_id      (arrival_id)
#  index_routes_on_departure_id    (departure_id)
#

class Route < ApplicationRecord
  belongs_to :departure,   class_name: 'Airport'
  belongs_to :arrival,     class_name: 'Airport'
  belongs_to :alternative, class_name: 'Airport'

  validates :description, length: { maximum: 2000}
  validates :duration, :flight, :alternative, :arrival, :departure, presence: true
end
