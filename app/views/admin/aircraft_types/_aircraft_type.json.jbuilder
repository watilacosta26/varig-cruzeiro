json.extract! aircraft_type, :id, :description, :created_at, :updated_at
json.url aircraft_type_url(aircraft_type, format: :json)
