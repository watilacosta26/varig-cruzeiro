# == Schema Information
#
# Table name: airports
#
#  id          :bigint           not null, primary key
#  description :string
#  icao        :string
#  lat         :string
#  long        :string
#  created_at  :datetime         not null
#  updated_at  :datetime         not null
#

class Airport < ApplicationRecord
  has_many :departure_route, class_name: 'Route', foreign_key: 'departure_id'
  has_many :arrival_route,   class_name: 'Route', foreign_key: 'arrival_id'
  has_many :alternative,     class_name: 'Route', foreign_key: 'alternative_id'
end
